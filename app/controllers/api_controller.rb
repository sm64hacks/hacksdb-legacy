class ApiController < ApplicationController

	def get_hack_count
		count = Hack.count || 0
		render :json => {
			count: count,
		}
	end

	def get_hack_data
		hack = Hack.find_by_id(params[:id])
		render :json => hack
	end

	def get_hack_version_count
		count = Download.where(hack_id: params[:id]).count
		render :json => count
	end

	def get_hack_version_data
		version = Download.where(hack_id: params[:id], id: params[:vid]).first
		render :json => version
	end
end
