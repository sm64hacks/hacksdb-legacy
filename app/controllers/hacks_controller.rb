class HacksController < ApplicationController
    before_action :require_user, only: [:add, :create, :edit, :save, :add_version, :create_version, :edit_version, :save_version, :delete, :confirm_delete, :delete_version, :confirm_delete_version]
    before_action :require_admin, only: [:add, :create, :edit, :save, :add_version, :create_version, :edit_version, :save_version, :delete, :confirm_delete, :delete_version, :confirm_delete_version]

    def all
      @page_title = "All Hacks"
      @hacks = Hack.order(:name).where(is_deleted: false).where.not(description: 'There is nothing for this ID in the old database')
    end

    def random
      @random = Hack.order("RANDOM()").where(is_deleted: false).where.not(description: 'There is nothing for this ID in the old database').first
      if @random
        redirect_to "/hack/#{@random.id}"
      else
        redirect_to "/error"
      end
    end

    def view
        @hack = Hack.where(id: params[:id], is_deleted: false).first
        @downloads = Download.where(hack_id: params[:id]).where(is_deleted: false)

        if (params[:id] == '404')
            render :template => 'hacks/easter_egg_404'
        elsif !@hack
          redirect_to "/error"
        elsif @hack == nil
          redirect_to "/error"
        else
          @page_title = @hack.name
        end
    end

    def add
        @page_title = "Add Hack"
        @hack = Hack.new
    end

    def create
      @hack = Hack.new(hack_params)

      if @hack.save
        redirect_to "/hack/#{@hack.id}"
      else 
        redirect_to "/error"
      end
    end

    def edit
      @page_title = "Edit Hack"
      @hack = Hack.where(id: params[:id], is_deleted: false).first
      if !@hack
        redirect_to "/error"
      end
    end

    def save
      @hack = Hack.where(id: params[:id], is_deleted: false).first
      @hack.name = params[:hack][:name]
      @hack.description = params[:hack][:description]
      @hack.creator = params[:hack][:creator]
      @hack.download_url = params[:hack][:download_url]
      @hack.youtube_url = params[:hack][:youtube_url]
      @hack.fandom_link = params[:hack][:fandom_link]
      @hack.difficulty = params[:hack][:difficulty]
      @hack.total_stars = params[:hack][:total_stars]
      @hack.required_stars = params[:hack][:required_stars]
      if @hack.save
        redirect_to "/hack/#{@hack.id}"
      else
        redirect_to "/error"
      end
    end

    def add_version
      @page_title = "Add Hack Version"
      @hack = Hack.where(id: params[:id], is_deleted: false).first
      if !@hack
        redirect_to "/error"
      end
    end

    def create_version
      @version = Download.new(version_params)

      if @version.save
        redirect_to "/hack/#{@version.hack_id}"
      else
        redirect_to "/error"
      end
    end

    def edit_version
      @page_title = "Edit Hack Version"
      @download = Download.where(id: params[:id], is_deleted: false).first
      if !@download
        redirect_to "/error"
      else
        @hack = Hack.where(id: @download.hack_id, is_deleted: false).first
        if !@hack
          redirect_to "/error"
        end
      end
    end

    def save_version
      @download = Download.where(id: params[:id], is_deleted: false).first
      @download.version = params[:download][:version]
      @download.comment = params[:download][:comment]
      @download.contributors = params[:download][:contributors]
      @download.everdrive = params[:download][:everdrive]
      @download.release_date = params[:download][:release_date]
      @download.download_url = params[:download][:download_url]
      if @download.save
        redirect_to "/hack/#{@download.hack_id}"
      else
        redirect_to "/error"
      end
    end

    def delete_version
      @page_title = "Delete Hack Version"
      @download = Download.where(id: params[:id], is_deleted: false).first
      if !@download
        redirect_to "/error"
      else
        @hack = Hack.where(id: @download.hack_id, is_deleted: false).first
        if !@hack
          redirect_to "/error"
        end
      end
    end

    def confirm_delete_version
      @download = Download.where(id: params[:id], is_deleted: false).first
      @hack = Hack.where(id: @download.hack_id, is_deleted: false).first
      @download.is_deleted = true
      @download.save
      redirect_to "/hack/#{@hack.id}"
    end

    def delete
      @page_title = "Delete Hack"
      @hack = Hack.where(id: params[:id], is_deleted: false).first
      if !@hack
        redirect_to "/error"
      end
    end

    def confirm_delete
      @hack = Hack.where(id: params[:id], is_deleted: false).first
      @downloads = Download.where(hack_id: @hack.id, is_deleted: false)
      @downloads.each {
        |download|
        download.is_deleted = true
        download.save
      }
      @hack.is_deleted = true
      @hack.save
      redirect_to "/"
    end

    def add_report
      @page_title = "Report Hack"
      @hack = Hack.where(id: params[:id], is_deleted: false).first
      if !@hack
        redirect_to "/error"
      end
    end

    def create_report
      @report = Report.new(report_params)
      @report.ipaddress = request.remote_ip
      @report.read = false

      if @report.save
        redirect_to '/'
      else
        redirect_to '/error'
      end
    end

    private
    def hack_params
	    params.require(:hack).permit(:name, :description, :creator, :download_url, :youtube_url, :difficulty, :total_stars, :required_stars, :fandom_link)
    end

    def version_params
      params.require(:download).permit(:version, :comment, :contributors, :everdrive, :release_date, :download_url, :hack_id)
    end

    def report_params
      params.require(:report).permit(:reason, :hack_id)
    end
end
