class AdminController < ApplicationController
    before_action :require_user
    before_action :require_admin

    def index
        @page_title = "Admin Area"
    end

    def list_users
        @page_title = "Admin Area - List Users"
        @users = User.all
    end

    def change_user_password
        @page_title = "Admin Area - Change Password"
    end

    def save_change_user_password
        @user = User.find_by_id(params[:id])
        @user.password = params[:user][:password]
        @user.save
        redirect_to "/"
    end

    def delete_user
        @page_title = "Admin Area - Delete User"
    end

    def confirm_delete_user
        if (params[:id].to_i == session[:user_id].to_i)
            redirect_to "/error"
        else
            @user = User.find_by_id(params[:id])
            @user.destroy
            redirect_to "/admin/user/list"
        end
    end

    def grant_admin
        @page_title = "Admin Area - Grant Admin"
    end

    def confirm_grant_admin
        @user = User.find_by_id(params[:id])
        @user.admin = true
        @user.save
        redirect_to "/admin/user/list"
    end

    def revoke_admin
        @page_title = "Admin Area - Revoke Admin"
    end

    def confirm_revoke_admin
        @user = User.find_by_id(params[:id])
        @user.admin = false
        @user.save
        redirect_to "/admin/user/list"
    end

    def list_user_reports
        @page_title = "Admin Area - List User Reports"
        @reports = Report.all
    end

    def request_report_ip_address
        @page_title = "Admin Area - Request Report IP Address"
    end

    def submit_request_report_ip_address
        @data_request = DataRequest.new
        @data_request.reason = params[:data_request][:reason]
        @data_request.record_type = 2
        @data_request.record_id = params[:id]
        @data_request.user_id = current_user.id
        @data_request.save
        
        redirect_to "/requested/#{@data_request.id}"
    end

    def delete_user_report
        @page_title = "Admin Area - Delete User Report"
        @report = Report.find_by_id(params[:id])
    end

    def confirm_delete_user_report
        @report = Report.find_by_id(params[:id])
        @report.destroy
        redirect_to "/admin/reports/list"
    end

    def ban_reporter
        @page_title = "Admin Area - Ban IP Address & Delete All Reports"
    end

    def confirm_ban_reporter
        @report = Report.find_by_id(params[:id])

        ## TODO: Implement ban logic

        @reports = Report.where(ipaddress: @report.ipaddress)
        @reports.each {
            |report|
            report.destroy
        }
        redirect_to "/admin/reports/list"
    end

    def test
        @me = current_user
    end
end
