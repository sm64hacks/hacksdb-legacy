class CreateDownloads < ActiveRecord::Migration[5.2]
  def change
    create_table :downloads do |t|
      t.string :version
      t.string :comment
      t.string :contributors
      t.string :everdrive
      t.string :release_date
      t.references :hack, foreign_key: true

      t.timestamps
    end
  end
end
