class AddDownloadUrlToDownloads < ActiveRecord::Migration[5.2]
  def change
    add_column :downloads, :download_url, :string
  end
end
