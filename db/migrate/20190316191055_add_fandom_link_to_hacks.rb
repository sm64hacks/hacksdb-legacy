class AddFandomLinkToHacks < ActiveRecord::Migration[5.2]
  def change
    add_column :hacks, :fandom_link, :string
  end
end
