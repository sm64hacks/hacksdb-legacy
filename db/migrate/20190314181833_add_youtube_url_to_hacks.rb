class AddYoutubeUrlToHacks < ActiveRecord::Migration[5.2]
  def change
    add_column :hacks, :youtube_url, :string
  end
end
