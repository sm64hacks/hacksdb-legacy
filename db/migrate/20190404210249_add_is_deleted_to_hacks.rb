class AddIsDeletedToHacks < ActiveRecord::Migration[5.2]
  def change
    add_column :hacks, :is_deleted, :boolean
    add_column :downloads, :is_deleted, :boolean
  end
end
