class ChangeTableHacks < ActiveRecord::Migration[5.2]
  def change
    change_table :hacks do |t|
      t.change :difficulty, :string
      t.change :total_stars, :string
      t.change :required_stars, :string
    end
  end
end
