class AddDefaultValuesToAFewColumns < ActiveRecord::Migration[5.2]
  def change
    change_column :hacks, :is_deleted, :boolean, :default => false
    change_column :downloads, :is_deleted, :boolean, :default => false
  end
end
